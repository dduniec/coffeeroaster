// FanTask.h
#include <TaskManagerIO.h>

#ifndef _FANTASK_h
#define _FANTASK_h

#include "Settings.h"

class FanTaskClass : public BaseEvent
{
public:
	FanTaskClass(SettingsClass &settings);
	void exec() override;
	uint32_t timeOfNextCheck() override;
protected:
	SettingsClass& _settingsClass;
	double _waitTime = 1000;
};

#endif
