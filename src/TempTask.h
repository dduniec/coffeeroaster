// TempTask.h
#include <TaskManagerIO.h>
#include <max6675.h>

#ifndef _TEMPTASK_h
#define _TEMPTASK_h

#include "Settings.h"

class TempTaskClass : public BaseEvent
{
public:
	TempTaskClass(SettingsClass &settings, MAX6675 &thermocouple, MAX6675 &thermocouple2);
	void exec() override;
	uint32_t timeOfNextCheck() override;
protected:
	SettingsClass& _settingsClass;

	MAX6675 &_avgThermocouple;
	MAX6675 &_avgThermocouple2;

	int _readingsNumber = 8;

	int _lastReadingTime = 0;
	int _lastReadingCount = 0;

	float _lastReadingTemp = 0;
	float _lastReadingTemp2 = 0;

	float _readingAverageTemp = 0;
	float _readingAverageTemp2 = 0;

	double _waitTime = 210;
};

#endif
