#ifndef __SETTINGS_H__
#define __SETTINGS_H__

#include <Arduino.h>

class SettingsClass
{
public:
    int heaterPin = 0;
    int fanPin = 0;

	int powerResolution = 0;
	int percentagePower = 0;
	int heaterTimeFrameInSeconds = 0;

	int fanPercentagePower = 0;

	float tempInCelcius = 0;
	float tempInCelcius2 = 0;
};

#endif // __SETTINGS_H__