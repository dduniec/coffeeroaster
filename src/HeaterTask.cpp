#include "HeaterTask.h"

HeaterTaskClass::HeaterTaskClass(SettingsClass &settingsClass) : _settingsClass(settingsClass), _lastState(LOW)
{
	pinMode(_settingsClass.heaterPin, OUTPUT);

	digitalWrite(_settingsClass.heaterPin, LOW);
}

uint32_t HeaterTaskClass::timeOfNextCheck()
{
	setTriggered(true);
	return millisToMicros(_waitTime);
}

void HeaterTaskClass::exec()
{
	// Don't allow heater to run when fan below 25%
	if (_settingsClass.percentagePower > 0 && _settingsClass.fanPercentagePower < 25)
	{
		_settingsClass.percentagePower = 0;
	}

	if (_settingsClass.percentagePower > 0 && _lastState == LOW)
	{
		turnAndWait(getHighTime(), HIGH);
		return;
	}

	if (_settingsClass.percentagePower < 100 && _lastState == HIGH)
	{
		turnAndWait(getLowTime(), LOW);
		return;
	}

	if (_settingsClass.percentagePower == 100)
	{
		turnAndWait(_settingsClass.heaterTimeFrameInSeconds * 1000, HIGH);
	}

	if (_settingsClass.percentagePower == 0)
	{
		turnAndWait(_settingsClass.heaterTimeFrameInSeconds * 1000, LOW);
	}
}

void HeaterTaskClass::turnAndWait(int timeInMiliseconds, int state)
{
	digitalWrite(_settingsClass.heaterPin, state);

	_lastState = state;
	_waitTime = timeInMiliseconds;
}

int HeaterTaskClass::getLowTime()
{
	return _settingsClass.percentagePower * _settingsClass.heaterTimeFrameInSeconds * 1000 / _settingsClass.powerResolution;
}

int HeaterTaskClass::getHighTime()
{
	return (_settingsClass.powerResolution - _settingsClass.percentagePower) * _settingsClass.heaterTimeFrameInSeconds * 1000 / _settingsClass.powerResolution;
}
