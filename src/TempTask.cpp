#include "TempTask.h"

TempTaskClass::TempTaskClass(SettingsClass &settingsClass, MAX6675 &thermocouple, MAX6675 &thermocouple2) : _settingsClass(settingsClass), _avgThermocouple(thermocouple), _avgThermocouple2(thermocouple2)
{
}

uint32_t TempTaskClass::timeOfNextCheck()
{
   setTriggered(true);
   return millisToMicros(_waitTime);
}

void TempTaskClass::exec()
{
   float temp = _avgThermocouple.readCelsius();
   float temp2 = _avgThermocouple2.readCelsius();
   _lastReadingCount++;

   // Skip reading if temp is higher than last reading + 30% 
   if (temp > _lastReadingTemp * 1.3 && _lastReadingTemp != 0)
   {
      temp = _lastReadingTemp;
   }

   // Skip reading if temp is higher than last reading + 30% 
   if (temp2  > _lastReadingTemp2 * 1.3 && _lastReadingTemp != 0)
   {
      temp2 = _lastReadingTemp2;
   }

   _lastReadingTemp = temp;
   _lastReadingTemp2 = temp2;

   _readingAverageTemp += temp;
   _readingAverageTemp2 += temp2;

   if (_lastReadingCount == _readingsNumber)
   {
      _settingsClass.tempInCelcius = _readingAverageTemp / _readingsNumber;
      _settingsClass.tempInCelcius2 = _readingAverageTemp2 / _readingsNumber;

      _readingAverageTemp = 0;
      _readingAverageTemp2 = 0;
      _lastReadingCount = 0;

      Serial.print(" Temp: ");
      Serial.print(_settingsClass.tempInCelcius);
      Serial.print(" ( ");
      Serial.print(temp);
      Serial.print("), ");
      Serial.print(_settingsClass.tempInCelcius2);
      Serial.print(" ( ");
      Serial.print(temp2);
      Serial.println(")");
   }
}