#include "FanTask.h"

FanTaskClass::FanTaskClass(SettingsClass &settingsClass) : _settingsClass(settingsClass)
{
	analogWrite(_settingsClass.fanPin, 0);
}

uint32_t FanTaskClass::timeOfNextCheck()
{
	setTriggered(true);
	return millisToMicros(_waitTime);
}

void FanTaskClass::exec()
{
	float multipler = _settingsClass.fanPercentagePower * 0.01;
	float pwmFloat = multipler * PWMRANGE;
	int pwmValue = (int)pwmFloat;

	// Serial.print("Fan power: ");
	// Serial.println(_settingsClass.fanPercentagePower);

	// Serial.print("Fan pwm: ");
	// Serial.println(pwmValue);

	// Serial.print("Heater power: ");
	// Serial.println(_settingsClass.percentagePower);

	analogWrite(_settingsClass.fanPin, pwmValue);
}
