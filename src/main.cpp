#include "Settings.h"
#include <TaskManagerIO.h>
#include "HeaterTask.h"
#include "TempTask.h"
#include "FanTask.h"

#include <ModbusRTU.h>

#include <max6675.h>

const int TEMP_HREG = 1;
const int TEMP2_HREG = 2;

const int HEAT_HREG = 3;
const int FAN_HREG = 4;

const int FAN_PIN = D1;
const int HEATER_PIN = D0;
const int powerResolutions = 100;
const int powerPercentage = 0;
const int heaterTimeFrameInSeconds = 1;

const int TK_TEMP_SO = D6;
const int TK_TEMP_SCK = D5;

const int TK_TEMP_CS = D7;
const int TK_TEMP2_CS = D2;

MAX6675  thermocouple(TK_TEMP_SCK, TK_TEMP_CS, TK_TEMP_SO);
MAX6675  thermocouple2(TK_TEMP_SCK, TK_TEMP2_CS, TK_TEMP_SO);

ModbusRTU mb;

auto *settings = new SettingsClass();

void setup()
{
  Serial.begin(9600);

  mb.begin(&Serial);
  mb.slave(1);

  settings->heaterPin = HEATER_PIN;
  settings->fanPin = FAN_PIN;
  settings->powerResolution = powerResolutions;
  settings->percentagePower = powerPercentage;
  settings->percentagePower = powerPercentage;
  settings->heaterTimeFrameInSeconds = heaterTimeFrameInSeconds;

  static auto *heaterTask = new HeaterTaskClass(*settings);
  static auto *tempTask = new TempTaskClass(*settings, thermocouple, thermocouple2);
  static auto *fanTask = new FanTaskClass(*settings);

  mb.addHreg(TEMP_HREG, 0);
  mb.addHreg(HEAT_HREG, 0);
  mb.addHreg(FAN_HREG, 0);
  mb.addHreg(TEMP2_HREG, 0);

  taskManager.registerEvent(heaterTask);
  taskManager.registerEvent(tempTask);
  taskManager.registerEvent(fanTask);
}

void loop()
{
  mb.task();

  int tempReading = settings->tempInCelcius * 100;
  int temp2Reading = settings->tempInCelcius2 * 100;

  mb.Hreg(TEMP_HREG, tempReading);
  mb.Hreg(TEMP2_HREG, temp2Reading);

  settings->percentagePower = mb.Hreg(HEAT_HREG);
  settings->fanPercentagePower = mb.Hreg(FAN_HREG);

  taskManager.runLoop();

  delay(10);
}